r = [
"Step C must be finished before step A can begin.",
"Step C must be finished before step F can begin.",
"Step A must be finished before step B can begin.",
"Step A must be finished before step D can begin.",
"Step B must be finished before step E can begin.",
"Step D must be finished before step E can begin.",
"Step F must be finished before step E can begin.",
]

f = [
"Step F must be finished before step R can begin.",
"Step I must be finished before step P can begin.",
"Step C must be finished before step O can begin.",
"Step H must be finished before step K can begin.",
"Step Y must be finished before step N can begin.",
"Step M must be finished before step J can begin.",
"Step D must be finished before step W can begin.",
"Step B must be finished before step N can begin.",
"Step T must be finished before step A can begin.",
"Step R must be finished before step L can begin.",
"Step P must be finished before step S can begin.",
"Step O must be finished before step J can begin.",
"Step X must be finished before step N can begin.",
"Step A must be finished before step K can begin.",
"Step N must be finished before step G can begin.",
"Step W must be finished before step U can begin.",
"Step Q must be finished before step U can begin.",
"Step V must be finished before step U can begin.",
"Step J must be finished before step G can begin.",
"Step G must be finished before step S can begin.",
"Step Z must be finished before step U can begin.",
"Step U must be finished before step S can begin.",
"Step E must be finished before step L can begin.",
"Step K must be finished before step L can begin.",
"Step L must be finished before step S can begin.",
"Step M must be finished before step N can begin.",
"Step T must be finished before step E can begin.",
"Step J must be finished before step U can begin.",
"Step G must be finished before step L can begin.",
"Step D must be finished before step P can begin.",
"Step T must be finished before step Z can begin.",
"Step U must be finished before step L can begin.",
"Step Z must be finished before step K can begin.",
"Step Q must be finished before step V can begin.",
"Step G must be finished before step K can begin.",
"Step Z must be finished before step E can begin.",
"Step Q must be finished before step Z can begin.",
"Step J must be finished before step S can begin.",
"Step G must be finished before step U can begin.",
"Step I must be finished before step M can begin.",
"Step W must be finished before step K can begin.",
"Step Y must be finished before step V can begin.",
"Step B must be finished before step Q can begin.",
"Step Y must be finished before step D can begin.",
"Step I must be finished before step G can begin.",
"Step A must be finished before step S can begin.",
"Step X must be finished before step S can begin.",
"Step O must be finished before step N can begin.",
"Step M must be finished before step X can begin.",
"Step V must be finished before step Z can begin.",
"Step W must be finished before step Z can begin.",
"Step C must be finished before step L can begin.",
"Step Q must be finished before step G can begin.",
"Step A must be finished before step U can begin.",
"Step G must be finished before step Z can begin.",
"Step P must be finished before step Q can begin.",
"Step C must be finished before step Z can begin.",
"Step U must be finished before step K can begin.",
"Step Q must be finished before step L can begin.",
"Step X must be finished before step U can begin.",
"Step A must be finished before step N can begin.",
"Step N must be finished before step S can begin.",
"Step Z must be finished before step L can begin.",
"Step F must be finished before step D can begin.",
"Step D must be finished before step A can begin.",
"Step J must be finished before step K can begin.",
"Step W must be finished before step Q can begin.",
"Step T must be finished before step J can begin.",
"Step T must be finished before step W can begin.",
"Step E must be finished before step K can begin.",
"Step P must be finished before step U can begin.",
"Step O must be finished before step Z can begin.",
"Step D must be finished before step B can begin.",
"Step R must be finished before step J can begin.",
"Step O must be finished before step A can begin.",
"Step N must be finished before step E can begin.",
"Step D must be finished before step G can begin.",
"Step M must be finished before step Q can begin.",
"Step F must be finished before step W can begin.",
"Step T must be finished before step L can begin.",
"Step U must be finished before step E can begin.",
"Step X must be finished before step L can begin.",
"Step M must be finished before step G can begin.",
"Step Z must be finished before step S can begin.",
"Step F must be finished before step Y can begin.",
"Step N must be finished before step Z can begin.",
"Step T must be finished before step U can begin.",
"Step D must be finished before step O can begin.",
"Step H must be finished before step X can begin.",
"Step V must be finished before step E can begin.",
"Step M must be finished before step T can begin.",
"Step Y must be finished before step O can begin.",
"Step P must be finished before step E can begin.",
"Step C must be finished before step E can begin.",
"Step P must be finished before step L can begin.",
"Step M must be finished before step A can begin.",
"Step F must be finished before step T can begin.",
"Step I must be finished before step C can begin.",
"Step X must be finished before step Z can begin.",
"Step Y must be finished before step U can begin.",
"Step B must be finished before step E can begin.",
]

from collections import defaultdict

def part1():
	print("Bonjour, partie 1")

	require = defaultdict(list)

	result = ""

	for steps in f:
		if steps[5] not in require:
			require[steps[5]].append("")
		if steps[36] not in require:
			require[steps[36]].append("")
		require[steps[36]].append(steps[5])


	while require:

		possible = []

		for key, value in require.items():
			if value == [""]:
				possible.append(key)

		possible = sorted(possible)

		result = result + possible[0]

		require.pop(possible[0])

		for key, value in require.items():
			if possible[0] in value:
				value.remove(possible[0])

	print result

class worker:
	def __init__ (self):
		self.seconds_lefts = 0
		self.step = ""
		self.busy = False


def part2():
	print("Bonjour, partie 2")

	require = defaultdict(list)

	second = 0

	result = ""

	workers = []

	for i in range(0, 5):
		workers.append(worker())

	for steps in f:
		if steps[5] not in require:
			require[steps[5]].append("")
		if steps[36] not in require:
			require[steps[36]].append("")
		require[steps[36]].append(steps[5])

	isBusy = True

	while require or isBusy:

		isBusy = False

		possible = []

		for key, value in require.items():
			if value == [""]:
				possible.append(key)

		possible = sorted(possible)

		for i in range(0, 5):
			myWorker = workers[i]
			#Start of the second
			if not myWorker.busy and len(possible) > 0:
				myWorker.step = possible[0]
				myWorker.seconds_lefts = ord(possible[0]) - 4
				myWorker.busy = True
				require.pop(possible[0])
				possible.remove(possible[0])

			#end of the second
			if myWorker.busy:
				myWorker.seconds_lefts = myWorker.seconds_lefts - 1
				if myWorker.seconds_lefts == 0:
					myWorker.busy = False
					for key, value in require.items():
						if myWorker.step in value:
							value.remove(myWorker.step)

			if myWorker.busy :
				isBusy = True

		second = second + 1
		print require

	print second

	
if __name__ == '__main__': part2()

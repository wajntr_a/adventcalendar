r = [
"1, 1",
"1, 6",
"8, 3",
"3, 4",
"5, 5",
"8, 9",
]

f = [
"267, 196",
"76, 184",
"231, 301",
"241, 76",
"84, 210",
"186, 243",
"251, 316",
"265, 129",
"142, 124",
"107, 134",
"265, 191",
"216, 226",
"67, 188",
"256, 211",
"317, 166",
"110, 41",
"347, 332",
"129, 91",
"217, 327",
"104, 57",
"332, 171",
"257, 287",
"230, 105",
"131, 209",
"110, 282",
"263, 146",
"113, 217",
"193, 149",
"280, 71",
"357, 160",
"356, 43",
"321, 123",
"272, 70",
"171, 49",
"288, 196",
"156, 139",
"268, 163",
"188, 141",
"156, 182",
"199, 242",
"330, 47",
"89, 292",
"351, 329",
"292, 353",
"290, 158",
"167, 116",
"268, 235",
"124, 139",
"116, 119",
"142, 259",
]


def part1():
	print("Bonjour, partie 1")
	l = []
	mat = []

	maxX = 0

	dist = 0

	for pos in f:
		l.append(pos.split(', '))
	for pos in l:
		pos[0] = int(pos[0])
		pos[1] = int(pos[1])
		if pos[0] > maxX:
			maxX = pos[0]
		if pos[1] > maxX:
			maxX = pos[1]

	for y in range(0, maxX + 1):
		mat.append([])
		for x in range(0, maxX + 1):
			mat[y].append('.')

	for t in range(0, len(l)):
		mat[l[t][1]][l[t][0]] = t

	for y in range(0, maxX + 1):
		for x in range(0, maxX + 1):
			i = 0
			temp_result = []
			minDistance = maxX * maxX
			minResult = -1
			square = minDistance
			if mat[x][y] == '.':
				for t in range(0, len(l)):
					dist = abs(x - l[t][1]) + abs(y - l[t][0])
					temp_result.append(dist)
				for t in range(0, len(temp_result)):
					if temp_result[t] < minDistance:
						minDistance = temp_result[t]
						minResult = t
					elif temp_result[t] == minDistance:
						square = minDistance
				if square > minDistance:
					mat[x][y] = minResult
				else:
					mat[x][y] = '#'

	ignore = []
	for x in range(0, maxX + 1):
		one = mat[0][x]
		two = mat[x][0]
		three = mat[maxX][x]
		four = mat[x][maxX]
		if one not in ignore:
			ignore.append(one)
		if two not in ignore:
			ignore.append(two)

	D = {}
	for y in range(0, maxX + 1):
		for x in range(0, maxX + 1):
			if mat[x][y] not in ignore:
				if mat[x][y] not in D:
					D[mat[x][y]] = 0
				D[mat[x][y]] = D[mat[x][y]] + 1


	print D
	maxKey = 0
	maxValue = maxKey
	for key, value in D.items():
		if value > maxValue:
			maxValue = value
			maxKey = int(key)


	print mat[0]


def part2():
	print("Bonjour, partie 2")
	l = []
	mat = []

	maxX = 0

	dist = 0

	for pos in f:
		l.append(pos.split(', '))
	for pos in l:
		pos[0] = int(pos[0])
		pos[1] = int(pos[1])
		if pos[0] > maxX:
			maxX = pos[0]
		if pos[1] > maxX:
			maxX = pos[1]

	for y in range(0, maxX + 1):
		mat.append([])
		for x in range(0, maxX + 1):
			mat[y].append('.')

	#for t in range(0, len(l)):
	#	mat[l[t][1]][l[t][0]] = t


	result = 0

	for y in range(0, maxX + 1):
		for x in range(0, maxX + 1):
			i = 0
			temp_result = []
			minDistance = maxX * maxX
			minResult = -1
			square = minDistance
			dist = 0
			if mat[x][y] == '.':
				for t in range(0, len(l)):
					dist = dist + abs(x - l[t][1]) + abs(y - l[t][0])
					temp_result.append(dist)
				if dist < 10000:
					result = result + 1

	print result
	
if __name__ == '__main__': part2()
